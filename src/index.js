import CompoundMetadata from './eubopen/CompoundMetadata.js';

const MetadataTagsGenerator = {
  EUbOPEN: {
    CompoundMetadata,
  },
};

export default MetadataTagsGenerator;
