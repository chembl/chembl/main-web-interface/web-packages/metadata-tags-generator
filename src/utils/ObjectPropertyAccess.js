const methods = {
  getPropertyValue(
    theObj,
    strProperty,
    defaultNullValue = '',
    returnUndefined = false
  ) {
    const propParts = strProperty.split('.')
    const currentProp = propParts[0]

    if (propParts.length > 1) {
      const currentObj = theObj[currentProp]
      if (currentObj == null) {
        if (returnUndefined) {
          return undefined
        }
        return defaultNullValue
      }
      return methods.getPropertyValue(
        currentObj,
        propParts.slice(1).join('.'),
        defaultNullValue,
        returnUndefined
      )
    }

    let value = theObj[currentProp]
    if (returnUndefined && value == null) {
      return undefined
    }
    value = value == null ? defaultNullValue : value
    return value
  },
}

export default methods
