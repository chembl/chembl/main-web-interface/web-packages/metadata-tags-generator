import ESProxyService from '@chembl/chembl-elasticsearch-service';
import ObjectPropertyAccess from '../utils/ObjectPropertyAccess.js';

const methods = {
  getSynonymsString(rawSynonyms) {
    if (rawSynonyms == null) {
      return '';
    }
    let synonyms = rawSynonyms.map((rawSyn) => rawSyn.synonyms);
    synonyms = [...new Set(synonyms)];
    return synonyms.join(', ');
  },
  getImgURL(itemID, compoundData, imgBaseURL, fallbackImgBaseURL) {
    const placeHolderImg = ObjectPropertyAccess.getPropertyValue(
      compoundData,
      '_metadata.compound_generated.image_file',
      null,
      true
    );
    return placeHolderImg == null
      ? `${imgBaseURL}/${itemID}.svg`
      : `${fallbackImgBaseURL}/${placeHolderImg}`;
  },
  getMolecularFormula(compoundData) {
    return ObjectPropertyAccess.getPropertyValue(
      compoundData,
      'molecule_properties.full_molformula',
      null,
      true
    );
  },
  getMolecularWeight(compoundData) {
    return ObjectPropertyAccess.getPropertyValue(
      compoundData,
      'molecule_properties.full_mwt',
      null,
      true
    );
  },
  getMonoisotopicMolecularWeight(compoundData) {
    return ObjectPropertyAccess.getPropertyValue(
      compoundData,
      'molecule_properties.mw_monoisotopic',
      null,
      true
    );
  },
  getSmiles(compoundData) {
    return ObjectPropertyAccess.getPropertyValue(
      compoundData,
      'molecule_structures.canonical_smiles',
      null,
      true
    );
  },
  getBioschemasMetadata({ itemID, itemURL, imgBaseURL, fallbackImgBaseURL }) {
    const indexName = 'chembl_eubopen_molecule';
    const docSource = [
      'pref_name',
      'molecule_synonyms',
      '_metadata.compound_generated.image_file',
      'molecule_properties.full_molformula',
      'molecule_properties.full_mwt',
      'molecule_properties.mw_monoisotopic',
      'molecule_structures.canonical_smiles'
    ];

    const baseMetadataObj = {
      '@context': 'http://schema.org',
      '@type': 'MolecularEntity',
      identifier: itemID,
      url: itemURL,
    };

    const metadataPromise = new Promise((resolve, reject) => {
      ESProxyService.getESDocument(indexName, itemID, docSource)
        .then((response) => {
          const dataReceived = response.data._source;
          const fullMetadata = {
            ...baseMetadataObj,
            name: dataReceived.pref_name,
            alternateName: methods.getSynonymsString(
              dataReceived.molecule_synonyms
            ),
            image: methods.getImgURL(
              itemID,
              dataReceived,
              imgBaseURL,
              fallbackImgBaseURL
            ),
            molecularFormula: methods.getMolecularFormula(dataReceived),
            molecularWeight: methods.getMolecularWeight(dataReceived),
            smiles: methods.getSmiles(dataReceived),
            monoisotopicMolecularWeight: methods.getMonoisotopicMolecularWeight(dataReceived),
            
          };
          resolve(fullMetadata);
        })
        .catch((error) => {
          reject(error);
        });
    });

    return metadataPromise;
  },
};

export default methods;
