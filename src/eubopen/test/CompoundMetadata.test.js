import CompoundMetadata from '../CompoundMetadata.js';
// eslint-disable-next-line no-undef
describe('CompoundMetadata', () => {
  // eslint-disable-next-line no-undef
  test('Generates the basic bioschemas metadata for a compound', () => {
    const itemID = 'CHEMBL4580220';
    const itemURL = `https://gateway.eubopen.org/compound/${itemID}`;
    const metadataObjMustBe = {
      '@context': 'http://schema.org',
      '@type': 'MolecularEntity',
      identifier: itemID,
      url: itemURL,
      name: 'BAY-3827',
    };
    return CompoundMetadata.getBioschemasMetadata({ itemID, itemURL }).then(
      (response) => {
        const metadataObtained = response;
        for (let prop of ['@context', '@type', 'identifier', 'url', 'name']) {
          // eslint-disable-next-line no-undef
          expect(metadataObtained[prop]).toBe(metadataObjMustBe[prop]);
        }
      }
    );
  });

  // eslint-disable-next-line no-undef
  test('Adds the alternate name in the schema for a compound', () => {
    const itemID = 'CHEMBL454138';
    const itemURL = `https://gateway.eubopen.org/compound/${itemID}`;
    const alternateNameMustBe = [
      'FXR-450',
      'turofexorate isopropyl',
      'TUROFEXORATE ISOPROPYL',
      'WAY-362450',
      'XL335',
      'XL-335',
    ].join(', ');

    return CompoundMetadata.getBioschemasMetadata({ itemID, itemURL }).then(
      (response) => {
        const metadataObtained = response;
        // eslint-disable-next-line no-undef
        expect(metadataObtained.alternateName).toBe(alternateNameMustBe);
      }
    );
  });

  // eslint-disable-next-line no-undef
  test('Adds the image for a compound with image', () => {
    const itemID = 'CHEMBL454138';
    const itemURL = `https://gateway.eubopen.org/compound/${itemID}`;
    const imgBaseURL = 'https://www.ebi.ac.uk/chembl/api/data/image';
    const fallbackImgBaseURL = '';
    const imgURLMustBe = `${imgBaseURL}/${itemID}.svg`;

    return CompoundMetadata.getBioschemasMetadata({
      itemID,
      itemURL,
      imgBaseURL,
      fallbackImgBaseURL,
    }).then((response) => {
      const metadataObtained = response;
      // eslint-disable-next-line no-undef
      expect(metadataObtained.image).toBe(imgURLMustBe);
    });
  });

  // It looks like there are no iupac
  // GET chembl_eubopen_molecule/_search
  // {
  //   "query": {
  //     "terms": {
  //       "molecule_synonyms.syn_type": [
  //         "SYSTEMATIC"
  //       ]
  //     }
  //   }
  // }

  // eslint-disable-next-line no-undef
  test('Adds the molecular formula to the metadata', () => {
    const itemID = 'CHEMBL454138';
    const itemURL = `https://gateway.eubopen.org/compound/${itemID}`;
    const imgBaseURL = 'https://www.ebi.ac.uk/chembl/api/data/image';
    const fallbackImgBaseURL = '';
    const molFormulaMustBe = 'C25H24F2N2O3';

    return CompoundMetadata.getBioschemasMetadata({
      itemID,
      itemURL,
      imgBaseURL,
      fallbackImgBaseURL,
    }).then((response) => {
      const metadataObtained = response;
      // eslint-disable-next-line no-undef
      expect(metadataObtained.molecularFormula).toBe(molFormulaMustBe);
    });
  });

  // eslint-disable-next-line no-undef
  test('Adds the molecular weight to the metadata', () => {
    const itemID = 'CHEMBL454138';
    const itemURL = `https://gateway.eubopen.org/compound/${itemID}`;
    const imgBaseURL = 'https://www.ebi.ac.uk/chembl/api/data/image';
    const fallbackImgBaseURL = '';
    const molWtMustBe = '438.47';

    return CompoundMetadata.getBioschemasMetadata({
      itemID,
      itemURL,
      imgBaseURL,
      fallbackImgBaseURL,
    }).then((response) => {
      const metadataObtained = response;
      // eslint-disable-next-line no-undef
      expect(metadataObtained.molecularWeight).toBe(molWtMustBe);
    });
  });

  // eslint-disable-next-line no-undef
  test('Adds the cannonical smiles to the metadata', () => {
    const itemID = 'CHEMBL454138';
    const itemURL = `https://gateway.eubopen.org/compound/${itemID}`;
    const imgBaseURL = 'https://www.ebi.ac.uk/chembl/api/data/image';
    const fallbackImgBaseURL = '';
    const smilesMustBe =
      'CC(C)OC(=O)C1=CN(C(=O)c2ccc(F)c(F)c2)CC(C)(C)c2c1[nH]c1ccccc21';

    return CompoundMetadata.getBioschemasMetadata({
      itemID,
      itemURL,
      imgBaseURL,
      fallbackImgBaseURL,
    }).then((response) => {
      const metadataObtained = response;
      // eslint-disable-next-line no-undef
      expect(metadataObtained.smiles).toBe(smilesMustBe);
    });
  });

  // eslint-disable-next-line no-undef
  test('Adds the cannonical smiles to the metadata', () => {
    const itemID = 'CHEMBL454138';
    const itemURL = `https://gateway.eubopen.org/compound/${itemID}`;
    const imgBaseURL = 'https://www.ebi.ac.uk/chembl/api/data/image';
    const fallbackImgBaseURL = '';
    const smilesMustBe =
      'CC(C)OC(=O)C1=CN(C(=O)c2ccc(F)c(F)c2)CC(C)(C)c2c1[nH]c1ccccc21';

    return CompoundMetadata.getBioschemasMetadata({
      itemID,
      itemURL,
      imgBaseURL,
      fallbackImgBaseURL,
    }).then((response) => {
      const metadataObtained = response;
      // eslint-disable-next-line no-undef
      expect(metadataObtained.smiles).toBe(smilesMustBe);
    });
  });

  // eslint-disable-next-line no-undef
  test('Adds the monoisotopic molecular weight to the metadata', () => {
    const itemID = 'CHEMBL454138';
    const itemURL = `https://gateway.eubopen.org/compound/${itemID}`;
    const imgBaseURL = 'https://www.ebi.ac.uk/chembl/api/data/image';
    const fallbackImgBaseURL = '';
    const molWtMonoisotopicMustBe = '438.1755';

    return CompoundMetadata.getBioschemasMetadata({
      itemID,
      itemURL,
      imgBaseURL,
      fallbackImgBaseURL,
    }).then((response) => {
      const metadataObtained = response;
      // eslint-disable-next-line no-undef
      expect(metadataObtained.monoisotopicMolecularWeight).toBe(
        molWtMonoisotopicMustBe
      );
    });
  });
});
